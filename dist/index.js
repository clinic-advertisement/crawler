"use strict";

var _district = require("./crawlers/district");

var _district2 = _interopRequireDefault(_district);

var _clinic = require("./crawlers/clinic");

var _clinic2 = _interopRequireDefault(_clinic);

var _fs = require("fs");

var _fs2 = _interopRequireDefault(_fs);

var _path = require("path");

var _path2 = _interopRequireDefault(_path);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(async function () {
  /**
   * Data return
   *  districtName
   *  districtNameSlugs
   */
  let districtClinicData = null;

  try {
    districtClinicData = await (0, _district2.default)();
  } catch (err) {
    debugger;
  }

  const promises = [];
  /**
   * CrawlDistrict clininc
   * Parallel crawling
   */

  for (let districtData of districtClinicData) {
    let currentDistrictData = districtData;
    promises.push(new Promise(function (resolve) {
      (0, _clinic2.default)(districtData.slug).then(async function districtClinicData(districtClinicData) {
        // extract address into
        currentDistrictData.clinics = districtClinicData;
        resolve();
      }).catch(err => {
        debugger;
      });
    }));
  } // Wait until data is fetching properly


  Promise.all(promises).then(() => {
    // Save data into json
    _fs2.default.writeFileSync(_path2.default.resolve(__dirname, 'districtClinicData.json'), JSON.stringify(districtClinicData)); // terminate script


    process.exit();
  });
})();
//# sourceMappingURL=index.js.map