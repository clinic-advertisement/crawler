# Clinic advertisement
* Web app
* Project info

## Dependencies
* Nightmare.js: best nodejs crawler in my opinion
* Babel + Present ENV: can't live with ES6 feature

## Notice
* Since this script is optimize to run in parallel. You may run out of RAM when trying to run this program. Please prepare at least free 10 gb RAM.

## Usage
* Step1: npm run build 
* Step2: node dist/index.js
* Step3: data crawled will be store in file dist/districtClinicData.json

## Crawler
* This serve as crawler for crawl clinic from the internet

## dependecies
* Chromeless: Interactive crawl the web

## Project structure
* crawlers: bunch of crawler
    * index.js: crawler data
    * utilities: their utilities
* config.js: config such as database connection
* database.js: return database instance
* dist: data crawled will be extract to data.json with format
eg:
Quan1: {
    clinics: [
        {
            name,
            avatar,
            address,
            longitude,
            latitude,
            description
        }
    ]
}