import crawlDistrict from './crawlers/district'
import crawlClinic from './crawlers/clinic'
import fs from 'fs'
import path from 'path'


(async function() {
    /**
     * Data return
     *  districtName
     *  districtNameSlugs
     */
    let districtClinicData = null
    try {
        districtClinicData = await crawlDistrict()
    } catch (err) {
        debugger
    }
    const promises = []

    /**
     * CrawlDistrict clininc
     * Parallel crawling
     */
    for (let districtData of districtClinicData) {
        let currentDistrictData = districtData 
        promises.push(
            new Promise(function (resolve) {
                crawlClinic(districtData.slug).then(async function districtClinicData (districtClinicData) {
                    // extract address into
                    currentDistrictData.clinics = districtClinicData
                    resolve()
                }).catch(err=>{ 
                    debugger
                })
            })
        )
    }

    // Wait until data is fetching properly
    Promise.all(promises).then(() => {
        // Save data into json
        fs.writeFileSync(
            path.resolve(__dirname, 'districtClinicData.json'),
            JSON.stringify(districtClinicData)
        )

        // terminate script
        process.exit()
    })
})()