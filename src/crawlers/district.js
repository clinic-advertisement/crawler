import superagent from 'superagent'


export default async function () {
    const data = await superagent
        .get('https://vicare.vn/api/v1/district/?provinces=ho-chi-minh')
    

    return data.body[0].data
}