import Nightmare from 'nightmare'
import {
    extractAddressIntoLngLat
} from './utils'

export default async function (districtSlugs) {
    var nightmare = new Nightmare({
        show: false
    })
    const transformedClinicCrawlDatas = await nightmare
        .goto(`https://vicare.vn/danh-sach/ho-chi-minh/quanhuyen-${districtSlugs}`)
        .evaluate(()=>{
            /**
             * Block clinic
             */
            const clinicNodes = document.querySelectorAll('li.has-actions.has-map-marker')
            const transformedClinicCrawlDatas = []
            
            // Remove unsed data
            document.querySelectorAll('.has-distance').forEach(e => e.parentNode.removeChild(e));

            for (let clinicNode of clinicNodes) {
                let transformedClinicCrawlData = {}

                // avatar
                transformedClinicCrawlData.avatar = clinicNode.querySelector('.hero-image').href

                // name
                transformedClinicCrawlData.name = clinicNode.querySelector("div.info>h2>a").innerText

                // address node
                let addressNode = clinicNode.querySelector("dl.brief>dd:first-of-type")
                
                // remove meta object
                
                transformedClinicCrawlData.address = addressNode.innerText

                // descriptions
                transformedClinicCrawlData.description = clinicNode.querySelector("dl.brief>dd:first-of-type").innerText

                // Push
                transformedClinicCrawlDatas.push(transformedClinicCrawlData)
            }

            return transformedClinicCrawlDatas
        })
        .end()
 

    // transform address into lng/lat
    const promises = []
    for (let transformedClinicCrawlData of transformedClinicCrawlDatas) {
        promises.push(
            new Promise(function (resolve) {
                extractAddressIntoLngLat(transformedClinicCrawlData.address).then(function (latlng) {
                    transformedClinicCrawlData.latlng = latlng
                    resolve()
                })
                .catch(err=>{
                    debugger
                })
        }))
    }

    // ^ Execute in parallel
    return new Promise(function (resolve) {
        Promise.all(promises).then(function () {
            resolve(transformedClinicCrawlDatas)
        })
    }) 
}
