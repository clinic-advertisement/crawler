import {
    googleMapApiKey
} from '../../config'

import superagent from 'superagent'

export const extractAddressIntoLngLat = (address) => {
    // fetch vào tìm kiếm địa điểm trong google map
    return new Promise((resolve)=>{
        superagent.get(`https://maps.googleapis.com/maps/api/geocode/json?address=${encodeURIComponent(address)}&key=${googleMapApiKey}`)
            .then(addressResult=>{
                resolve(addressResult.body.results[0].geometry.location)
            })
            .catch(err=>{
                console.err(err)
            })
    })
}